--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11
-- Dumped by pg_dump version 10.11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: visiting; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE visiting WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE visiting OWNER TO postgres;

\connect visiting

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: builds; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA builds;


ALTER SCHEMA builds OWNER TO postgres;

--
-- Name: personal; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA personal;


ALTER SCHEMA personal OWNER TO postgres;

--
-- Name: report; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA report;


ALTER SCHEMA report OWNER TO postgres;

--
-- Name: schedule; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA schedule;


ALTER SCHEMA schedule OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: position; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public."position" AS (
	latitude double precision,
	longitude double precision,
	altitude double precision
);


ALTER TYPE public."position" OWNER TO postgres;

--
-- Name: check_role(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.check_role() RETURNS text
    LANGUAGE plpgsql
    AS $$
declare
	_groups text[];
begin
	SELECT ARRAY(SELECT b.rolname::text
        FROM pg_catalog.pg_auth_members m
        JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid)
        WHERE m.member = (SELECT oid FROM pg_catalog.pg_roles WHERE rolname = current_role)) into _groups;
	
	IF (array_position(_groups, 'admins') IS NOT NULL)
	THEN
		RETURN 'admins';
	END IF;
	IF (array_position(_groups, 'teachers') IS NOT NULL)
	THEN
		RETURN 'teachers';
	END IF;
	IF (array_position(_groups, 'students') IS NOT NULL)
	THEN
		RETURN 'students';
	END IF;
	return 'error';
end;$$;


ALTER FUNCTION public.check_role() OWNER TO postgres;

--
-- Name: current_lesson_student(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.current_lesson_student() RETURNS TABLE(teacher text, subject_name text, corpus text, auditorium text)
    LANGUAGE sql
    AS $$
	select format('%s %s %s', t.last_name, t.first_name, t.patronymic)::text as teacher,
		subj.subject_name::text, a.corpus::text, a.auditorium::text
	from personal.students st
		left join personal.groups g ON st.group_number = g.group_number
		left join schedule.schedule sch ON g.group_number = sch.group_number
		left join schedule.subjects subj ON sch.subject_key = subj.subject_key
		left join personal.teachers t ON sch.teachers_key = t.teachers_key
		left join builds.auditoriums a ON sch.auditoriums_key = a.auditoriums_key
	WHERE st.login = current_user and 
		(current_time >= time_start and current_time <= time_end)
		and day_key = EXTRACT(DOW FROM current_date);
$$;


ALTER FUNCTION public.current_lesson_student() OWNER TO postgres;

--
-- Name: current_lesson_teacher(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.current_lesson_teacher() RETURNS TABLE(group_number text, subject_name text, corpus text, auditorium text)
    LANGUAGE sql
    AS $$
	SELECT array_to_string(x.group_number, ',') as group_number, x.subject_name::text, x.corpus, x.auditorium
	FROM
	(
		select array_agg(g.group_number::text) as group_number, subj.subject_name::text, a.corpus::text, a.auditorium::text
		from personal.teachers t
			left join schedule.schedule sch ON sch.teachers_key = t.teachers_key
			left join schedule.subjects subj ON sch.subject_key = subj.subject_key
			left join personal.groups g ON sch.group_number = g.group_number
			left join builds.auditoriums a ON sch.auditoriums_key = a.auditoriums_key
		WHERE t.login = current_user and 
			(current_time >= time_start and current_time <= time_end)
			and sch.day_key = EXTRACT(DOW FROM current_date)
		group by subj.subject_name::text, a.corpus::text, a.auditorium::text
	) x;
$$;


ALTER FUNCTION public.current_lesson_teacher() OWNER TO postgres;

--
-- Name: current_schedule_student(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.current_schedule_student() RETURNS TABLE("time" text, subject_name text)
    LANGUAGE sql
    AS $$
	select format('%s - %s',sch.time_start, sch.time_end)::text as "time",
		subj.subject_name::text as subject_name 
	from personal.students st
		left join personal.groups g ON st.group_number = g.group_number
		left join schedule.schedule sch ON g.group_number = sch.group_number
		left join schedule.subjects subj ON sch.subject_key = subj.subject_key
		left join personal.teachers t ON sch.teachers_key = t.teachers_key
		left join builds.auditoriums a ON sch.auditoriums_key = a.auditoriums_key
	WHERE st.login = current_user and 
		day_key = EXTRACT(DOW FROM current_date);
$$;


ALTER FUNCTION public.current_schedule_student() OWNER TO postgres;

--
-- Name: current_schedule_teacher(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.current_schedule_teacher() RETURNS TABLE("time" text, subject_name text)
    LANGUAGE sql
    AS $$
	select DISTINCT format('%s - %s',sch.time_start, sch.time_end)::text as "time",
		subj.subject_name::text as subject_name 
	from personal.teachers t
			left join schedule.schedule sch ON sch.teachers_key = t.teachers_key
			left join schedule.subjects subj ON sch.subject_key = subj.subject_key
			left join builds.auditoriums a ON sch.auditoriums_key = a.auditoriums_key
	WHERE t.login = current_user and 
		sch.day_key = EXTRACT(DOW FROM current_date);
$$;


ALTER FUNCTION public.current_schedule_teacher() OWNER TO postgres;

--
-- Name: end_lesson(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.end_lesson() RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
	_row RECORD;
begin
	UPDATE report.work_tabel SET working_end = CURRENT_TIMESTAMP
		WHERE schedule_key = (SELECT schedule_key FROM schedule.schedule WHERE teachers_key = 
							  (SELECT teachers_key FROM personal.teachers WHERE login = CURRENT_USER)
							 	AND (current_time >= time_start and current_time <= time_end + interval '5 minute')
								and day_key = EXTRACT(DOW FROM current_date));
end;$$;


ALTER FUNCTION public.end_lesson() OWNER TO postgres;

--
-- Name: next_lesson_student(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.next_lesson_student() RETURNS TABLE(teacher text, subject_name text, corpus text, auditorium text)
    LANGUAGE sql
    AS $$
	SELECT x.teacher, x.subject_name, x.corpus, x.auditorium
	FROM
	(
		select format('%s %s %s',st.last_name, st.first_name, st.patronymic)::text as student, g.group_number::text,
			format('%s %s %s', t.last_name, t.first_name, t.patronymic)::text as teacher,
			subj.subject_name::text, a.corpus::text, a.auditorium::text, (sch.time_start - current_time::time without time zone) as time_left
		from personal.students st
			left join personal.groups g ON st.group_number = g.group_number
			left join schedule.schedule sch ON g.group_number = sch.group_number
			left join schedule.subjects subj ON sch.subject_key = subj.subject_key
			left join personal.teachers t ON sch.teachers_key = t.teachers_key
			left join builds.auditoriums a ON sch.auditoriums_key = a.auditoriums_key
		WHERE st.login = current_user and 
			day_key = EXTRACT(DOW FROM current_date)
	) x
	WHERE x.time_left > '0:0:0'::time without time zone
	ORDER BY x.time_left ASC
	LIMIT 1;
$$;


ALTER FUNCTION public.next_lesson_student() OWNER TO postgres;

--
-- Name: next_lesson_teacher(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.next_lesson_teacher() RETURNS TABLE(group_number text, subject_name text, corpus text, auditorium text)
    LANGUAGE sql
    AS $$
	SELECT array_to_string(x.group_number, ',') as group_number, x.subject_name, x.corpus, x.auditorium
	FROM
	(
		select array_agg(g.group_number::text) as group_number, subj.subject_name::text, a.corpus::text, a.auditorium::text,
			(sch.time_start - current_time::time without time zone) as time_left
		from personal.teachers t
			left join schedule.schedule sch ON sch.teachers_key = t.teachers_key
			left join schedule.subjects subj ON sch.subject_key = subj.subject_key
			left join personal.groups g ON sch.group_number = g.group_number
			left join builds.auditoriums a ON sch.auditoriums_key = a.auditoriums_key
		WHERE t.login = current_user and 
			sch.day_key = EXTRACT(DOW FROM current_date)
		group by subj.subject_name::text, a.corpus::text, a.auditorium::text, time_left
	) x
	WHERE x.time_left > '0:0:0'::time without time zone
	ORDER BY x.time_left ASC
	LIMIT 1;
$$;


ALTER FUNCTION public.next_lesson_teacher() OWNER TO postgres;

--
-- Name: start_lesson(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.start_lesson() RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
	_row RECORD;
begin
	for _row IN
	(
		SELECT *
		FROM personal.teachers t
			LEFT JOIN schedule.schedule sch ON sch.teachers_key = t.teachers_key
			LEFT JOIN personal.groups g ON sch.group_number = g.group_number
			LEFT JOIN personal.students st ON g.group_number = st.group_number
		WHERE t.login = CURRENT_USER::text 
			AND sch.day_key::double precision = date_part('dow'::text, CURRENT_DATE) 
			AND CURRENT_TIME >= sch.time_start::time with time zone
			AND CURRENT_TIME <= sch.time_end::time with time zone
	)
	LOOP
		INSERT INTO report.visiting (student_key, schedule_key) VALUES (_row.student_key, _row.schedule_key);
	END LOOP;
	INSERT INTO report.work_tabel (schedule_key, working_start) VALUES (_row.schedule_key, CURRENT_TIMESTAMP);
end;$$;


ALTER FUNCTION public.start_lesson() OWNER TO postgres;

--
-- Name: tap_end(public."position"); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.tap_end(pos public."position") RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
	lat double precision;
	long double precision;
	alt double precision;
	_time_start time(6) without time zone;
	_time_end time(6) without time zone;
begin
	SELECT time_start INTO _time_start FROM schedule.schedule WHERE (current_time >= time_start and current_time <= time_end)
		AND group_number = (SELECT group_number FROM personal.students WHERE login = current_user);
		
	SELECT time_end INTO _time_end FROM schedule.schedule WHERE (current_time >= time_start and current_time <= time_end)
		AND group_number = (SELECT group_number FROM personal.students WHERE login = current_user);
		
	SELECT (coords).latitude, (coords).longitude, (coords).altitude INTO lat, long, alt
		FROM builds.auditoriums WHERE auditoriums_key = (SELECT auditoriums_key FROM schedule.schedule 
			WHERE (current_time >= time_start and current_time <= time_end) 
				AND group_number = (SELECT group_number FROM personal.students WHERE login = current_user));

	UPDATE report.visiting SET visit_end = current_timestamp::timestamp without time zone
	WHERE student_key = (SELECT student_key FROM personal.students WHERE login = current_user)
		AND (current_time >= _time_start and current_time <= _time_end + interval '5 minute')
		AND pos.latitude * 111 * 1000 >= lat * 111 * 1000 - 15 AND pos.latitude * 111 * 1000 <= lat * 111 * 1000 + 15
		AND pos.longitude * 111 * 1000 >= long * 111 * 1000 - 15 AND pos.longitude * 111 * 1000 <= long * 111 * 1000 + 15
		AND pos.altitude = alt;
end;$$;


ALTER FUNCTION public.tap_end(pos public."position") OWNER TO postgres;

--
-- Name: tap_start(public."position"); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.tap_start(pos public."position") RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
	lat double precision;
	long double precision;
	alt double precision;
	_time_start time(6) without time zone;
	_time_end time(6) without time zone;
begin
	SELECT time_start INTO _time_start FROM schedule.schedule WHERE (current_time >= time_start and current_time <= time_end)
		AND group_number = (SELECT group_number FROM personal.students WHERE login = current_user);
		
	SELECT time_end INTO _time_end FROM schedule.schedule WHERE (current_time >= time_start and current_time <= time_end)
		AND group_number = (SELECT group_number FROM personal.students WHERE login = current_user);
		
	SELECT (coords).latitude, (coords).longitude, (coords).altitude INTO lat, long, alt FROM builds.auditoriums 
		WHERE auditoriums_key = (SELECT auditoriums_key FROM schedule.schedule 
			WHERE (current_time >= time_start and current_time <= time_end) 
				AND group_number = (SELECT group_number FROM personal.students WHERE login = current_user));

	UPDATE report.visiting SET visit_start = current_timestamp::timestamp without time zone
	WHERE student_key = (SELECT student_key FROM personal.students WHERE login = current_user)
		AND (current_time >= _time_start and current_time <= _time_end)
		AND pos.latitude * 111 * 1000 >= lat * 111 * 1000 - 15 AND pos.latitude * 111 * 1000 <= lat * 111 * 1000 + 15
		AND pos.longitude * 111 * 1000 >= long * 111 * 1000 - 15 AND pos.longitude * 111 * 1000 <= long * 111 * 1000 + 15
		AND pos.altitude = alt;
end;$$;


ALTER FUNCTION public.tap_start(pos public."position") OWNER TO postgres;

--
-- Name: auditoriums_seq; Type: SEQUENCE; Schema: builds; Owner: postgres
--

CREATE SEQUENCE builds.auditoriums_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE builds.auditoriums_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auditoriums; Type: TABLE; Schema: builds; Owner: postgres
--

CREATE TABLE builds.auditoriums (
    auditoriums_key integer DEFAULT nextval('builds.auditoriums_seq'::regclass) NOT NULL,
    corpus text,
    auditorium text,
    coords public."position"
);


ALTER TABLE builds.auditoriums OWNER TO postgres;

--
-- Name: groups; Type: TABLE; Schema: personal; Owner: postgres
--

CREATE TABLE personal.groups (
    group_number text NOT NULL
);


ALTER TABLE personal.groups OWNER TO postgres;

--
-- Name: students_seq; Type: SEQUENCE; Schema: personal; Owner: postgres
--

CREATE SEQUENCE personal.students_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personal.students_seq OWNER TO postgres;

--
-- Name: students; Type: TABLE; Schema: personal; Owner: postgres
--

CREATE TABLE personal.students (
    student_key integer DEFAULT nextval('personal.students_seq'::regclass) NOT NULL,
    group_number text,
    last_name text,
    first_name text,
    patronymic text,
    login text
);


ALTER TABLE personal.students OWNER TO postgres;

--
-- Name: teachers_seq; Type: SEQUENCE; Schema: personal; Owner: postgres
--

CREATE SEQUENCE personal.teachers_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personal.teachers_seq OWNER TO postgres;

--
-- Name: teachers; Type: TABLE; Schema: personal; Owner: postgres
--

CREATE TABLE personal.teachers (
    teachers_key integer DEFAULT nextval('personal.teachers_seq'::regclass) NOT NULL,
    last_name text,
    first_name text,
    patronymic text,
    login text
);


ALTER TABLE personal.teachers OWNER TO postgres;

--
-- Name: visiting; Type: TABLE; Schema: report; Owner: postgres
--

CREATE TABLE report.visiting (
    visiting_key uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    student_key integer,
    schedule_key integer,
    visit_start timestamp without time zone,
    visit_end timestamp without time zone
);


ALTER TABLE report.visiting OWNER TO postgres;

--
-- Name: schedule_seq; Type: SEQUENCE; Schema: schedule; Owner: postgres
--

CREATE SEQUENCE schedule.schedule_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schedule.schedule_seq OWNER TO postgres;

--
-- Name: schedule; Type: TABLE; Schema: schedule; Owner: postgres
--

CREATE TABLE schedule.schedule (
    schedule_key integer DEFAULT nextval('schedule.schedule_seq'::regclass) NOT NULL,
    group_number text,
    teachers_key integer,
    subject_key integer,
    time_start time(6) without time zone,
    time_end time(6) without time zone,
    day_key integer,
    auditoriums_key integer
);


ALTER TABLE schedule.schedule OWNER TO postgres;

--
-- Name: subjects_seq; Type: SEQUENCE; Schema: schedule; Owner: postgres
--

CREATE SEQUENCE schedule.subjects_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schedule.subjects_seq OWNER TO postgres;

--
-- Name: subjects; Type: TABLE; Schema: schedule; Owner: postgres
--

CREATE TABLE schedule.subjects (
    subject_key integer DEFAULT nextval('schedule.subjects_seq'::regclass) NOT NULL,
    subject_name text
);


ALTER TABLE schedule.subjects OWNER TO postgres;

--
-- Name: current_visiting; Type: VIEW; Schema: report; Owner: postgres
--

CREATE VIEW report.current_visiting AS
 SELECT row_number() OVER (ORDER BY x.student) AS num,
    x.student,
    x.visit_time
   FROM ( SELECT format('%s %s %s'::text, st.last_name, st.first_name, st.patronymic) AS student,
            ((COALESCE((vis.visit_end)::time without time zone, (CURRENT_TIME)::time without time zone) - (vis.visit_start)::time without time zone))::text AS visit_time
           FROM ((((((report.visiting vis
             LEFT JOIN personal.students st ON ((vis.student_key = st.student_key)))
             LEFT JOIN personal.groups g ON ((st.group_number = g.group_number)))
             LEFT JOIN schedule.schedule sch ON ((sch.group_number = g.group_number)))
             LEFT JOIN schedule.subjects subj ON ((sch.subject_key = subj.subject_key)))
             LEFT JOIN personal.teachers t ON ((sch.teachers_key = t.teachers_key)))
             LEFT JOIN builds.auditoriums a ON ((sch.auditoriums_key = a.auditoriums_key)))
          WHERE ((t.login = (CURRENT_USER)::text) AND ((sch.day_key)::double precision = date_part('dow'::text, CURRENT_DATE)) AND (CURRENT_TIME >= (sch.time_start)::time with time zone) AND (CURRENT_TIME <= (sch.time_end)::time with time zone))) x
  ORDER BY x.student;


ALTER TABLE report.current_visiting OWNER TO postgres;

--
-- Name: work_tabel; Type: TABLE; Schema: report; Owner: postgres
--

CREATE TABLE report.work_tabel (
    work_tabel_key uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    schedule_key integer,
    working_start timestamp without time zone,
    working_end timestamp without time zone
);


ALTER TABLE report.work_tabel OWNER TO postgres;

--
-- Name: days; Type: TABLE; Schema: schedule; Owner: postgres
--

CREATE TABLE schedule.days (
    day_key integer NOT NULL,
    day text
);


ALTER TABLE schedule.days OWNER TO postgres;

--
-- Data for Name: auditoriums; Type: TABLE DATA; Schema: builds; Owner: postgres
--

INSERT INTO builds.auditoriums (auditoriums_key, corpus, auditorium, coords) VALUES (1, 'Главный', '510', '(510,510,510)');
INSERT INTO builds.auditoriums (auditoriums_key, corpus, auditorium, coords) VALUES (2, 'Главный', '508', '(508,508,508)');
INSERT INTO builds.auditoriums (auditoriums_key, corpus, auditorium, coords) VALUES (4, 'Главный', '322', '(322,322,322)');
INSERT INTO builds.auditoriums (auditoriums_key, corpus, auditorium, coords) VALUES (5, 'Главный', '326', '(326,326,326)');
INSERT INTO builds.auditoriums (auditoriums_key, corpus, auditorium, coords) VALUES (3, 'Главный', '418', '(418,418,418)');


--
-- Data for Name: groups; Type: TABLE DATA; Schema: personal; Owner: postgres
--

INSERT INTO personal.groups (group_number) VALUES ('220651');


--
-- Data for Name: students; Type: TABLE DATA; Schema: personal; Owner: postgres
--

INSERT INTO personal.students (student_key, group_number, last_name, first_name, patronymic, login) VALUES (2, '220651', 'Крюков', 'Олег', 'Сергеевич', 'kos');
INSERT INTO personal.students (student_key, group_number, last_name, first_name, patronymic, login) VALUES (3, '220651', 'Ягунова', 'Екатерина', 'Сергеевна', NULL);


--
-- Data for Name: teachers; Type: TABLE DATA; Schema: personal; Owner: postgres
--

INSERT INTO personal.teachers (teachers_key, last_name, first_name, patronymic, login) VALUES (1, 'Набродова', 'Ирина', 'Николаевна', 'nin');


--
-- Data for Name: visiting; Type: TABLE DATA; Schema: report; Owner: postgres
--

INSERT INTO report.visiting (visiting_key, student_key, schedule_key, visit_start, visit_end) VALUES ('c862d960-0269-4dc5-99c2-fbed23c8faa1', 2, 14, NULL, NULL);
INSERT INTO report.visiting (visiting_key, student_key, schedule_key, visit_start, visit_end) VALUES ('636bbc73-1fe9-4e52-85fc-8ced1490d615', 3, 14, NULL, NULL);


--
-- Data for Name: work_tabel; Type: TABLE DATA; Schema: report; Owner: postgres
--

INSERT INTO report.work_tabel (work_tabel_key, schedule_key, working_start, working_end) VALUES ('133403bf-b44e-499c-a57c-17bd5d3bbf1c', 14, '2019-11-16 09:42:06.119347', '2019-11-16 09:42:39.395761');


--
-- Data for Name: days; Type: TABLE DATA; Schema: schedule; Owner: postgres
--

INSERT INTO schedule.days (day_key, day) VALUES (0, 'Воскресение');
INSERT INTO schedule.days (day_key, day) VALUES (1, 'Понедельник');
INSERT INTO schedule.days (day_key, day) VALUES (2, 'Вторник');
INSERT INTO schedule.days (day_key, day) VALUES (3, 'Среда');
INSERT INTO schedule.days (day_key, day) VALUES (4, 'Четверг');
INSERT INTO schedule.days (day_key, day) VALUES (5, 'Пятница');
INSERT INTO schedule.days (day_key, day) VALUES (6, 'Суббота');


--
-- Data for Name: schedule; Type: TABLE DATA; Schema: schedule; Owner: postgres
--

INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (1, '220651', 1, 1, '09:40:00', '11:15:00', 1, 1);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (9, '220651', 1, 2, '11:35:00', '13:10:00', 1, 2);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (10, '220651', 1, 3, '13:40:00', '15:15:00', 1, 3);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (11, '220651', 1, 4, '15:35:00', '17:10:00', 1, 4);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (12, '220651', 1, 5, '17:30:00', '19:05:00', 1, 2);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (13, '220651', 1, 6, '07:45:00', '09:20:00', 6, 3);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (14, '220651', 1, 7, '09:40:00', '11:15:00', 6, 5);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (15, '220651', 1, 2, '11:35:00', '13:10:00', 6, 2);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (16, '220651', 1, 3, '13:40:00', '15:15:00', 6, 3);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (17, '220651', 1, 4, '15:35:00', '17:10:00', 6, 4);
INSERT INTO schedule.schedule (schedule_key, group_number, teachers_key, subject_key, time_start, time_end, day_key, auditoriums_key) VALUES (18, '220651', 1, 5, '17:30:00', '19:05:00', 6, 2);


--
-- Data for Name: subjects; Type: TABLE DATA; Schema: schedule; Owner: postgres
--

INSERT INTO schedule.subjects (subject_key, subject_name) VALUES (1, 'Структуры и алгоритмы обработки данных (л)');
INSERT INTO schedule.subjects (subject_key, subject_name) VALUES (2, 'Функциональное и логическое программирование (л)');
INSERT INTO schedule.subjects (subject_key, subject_name) VALUES (3, 'Основы моделирования систем (л)');
INSERT INTO schedule.subjects (subject_key, subject_name) VALUES (4, 'Структуры и алгоритмы обработки данных (лаб)');
INSERT INTO schedule.subjects (subject_key, subject_name) VALUES (5, 'Основы моделирования систем (лаб)');
INSERT INTO schedule.subjects (subject_key, subject_name) VALUES (6, 'Функциональное и логическое программирование (лаб)');
INSERT INTO schedule.subjects (subject_key, subject_name) VALUES (7, 'Теория алгоритмов и автоматов (л)');


--
-- Name: auditoriums_seq; Type: SEQUENCE SET; Schema: builds; Owner: postgres
--

SELECT pg_catalog.setval('builds.auditoriums_seq', 4, true);


--
-- Name: students_seq; Type: SEQUENCE SET; Schema: personal; Owner: postgres
--

SELECT pg_catalog.setval('personal.students_seq', 3, true);


--
-- Name: teachers_seq; Type: SEQUENCE SET; Schema: personal; Owner: postgres
--

SELECT pg_catalog.setval('personal.teachers_seq', 1, true);


--
-- Name: schedule_seq; Type: SEQUENCE SET; Schema: schedule; Owner: postgres
--

SELECT pg_catalog.setval('schedule.schedule_seq', 18, true);


--
-- Name: subjects_seq; Type: SEQUENCE SET; Schema: schedule; Owner: postgres
--

SELECT pg_catalog.setval('schedule.subjects_seq', 7, true);


--
-- Name: auditoriums auditoriums_pkey; Type: CONSTRAINT; Schema: builds; Owner: postgres
--

ALTER TABLE ONLY builds.auditoriums
    ADD CONSTRAINT auditoriums_pkey PRIMARY KEY (auditoriums_key);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: personal; Owner: postgres
--

ALTER TABLE ONLY personal.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (group_number);


--
-- Name: students students_pkey; Type: CONSTRAINT; Schema: personal; Owner: postgres
--

ALTER TABLE ONLY personal.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (student_key);


--
-- Name: teachers teachers_pkey; Type: CONSTRAINT; Schema: personal; Owner: postgres
--

ALTER TABLE ONLY personal.teachers
    ADD CONSTRAINT teachers_pkey PRIMARY KEY (teachers_key);


--
-- Name: visiting visiting_pkey; Type: CONSTRAINT; Schema: report; Owner: postgres
--

ALTER TABLE ONLY report.visiting
    ADD CONSTRAINT visiting_pkey PRIMARY KEY (visiting_key);


--
-- Name: work_tabel work_tabel_pkey; Type: CONSTRAINT; Schema: report; Owner: postgres
--

ALTER TABLE ONLY report.work_tabel
    ADD CONSTRAINT work_tabel_pkey PRIMARY KEY (work_tabel_key);


--
-- Name: days days_pkey; Type: CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.days
    ADD CONSTRAINT days_pkey PRIMARY KEY (day_key);


--
-- Name: schedule schedule_pkey; Type: CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (schedule_key);


--
-- Name: subjects subjects_pkey; Type: CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.subjects
    ADD CONSTRAINT subjects_pkey PRIMARY KEY (subject_key);


--
-- Name: students students_group_number_fkey; Type: FK CONSTRAINT; Schema: personal; Owner: postgres
--

ALTER TABLE ONLY personal.students
    ADD CONSTRAINT students_group_number_fkey FOREIGN KEY (group_number) REFERENCES personal.groups(group_number) NOT VALID;


--
-- Name: visiting visiting_schedule_key_fkey; Type: FK CONSTRAINT; Schema: report; Owner: postgres
--

ALTER TABLE ONLY report.visiting
    ADD CONSTRAINT visiting_schedule_key_fkey FOREIGN KEY (schedule_key) REFERENCES schedule.schedule(schedule_key);


--
-- Name: visiting visiting_student_key_fkey; Type: FK CONSTRAINT; Schema: report; Owner: postgres
--

ALTER TABLE ONLY report.visiting
    ADD CONSTRAINT visiting_student_key_fkey FOREIGN KEY (student_key) REFERENCES personal.students(student_key);


--
-- Name: work_tabel work_tabel_schedule_key_fkey; Type: FK CONSTRAINT; Schema: report; Owner: postgres
--

ALTER TABLE ONLY report.work_tabel
    ADD CONSTRAINT work_tabel_schedule_key_fkey FOREIGN KEY (schedule_key) REFERENCES schedule.schedule(schedule_key) NOT VALID;


--
-- Name: schedule schedule_auditoriums_key_fkey; Type: FK CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.schedule
    ADD CONSTRAINT schedule_auditoriums_key_fkey FOREIGN KEY (auditoriums_key) REFERENCES builds.auditoriums(auditoriums_key) NOT VALID;


--
-- Name: schedule schedule_day_key_fkey; Type: FK CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.schedule
    ADD CONSTRAINT schedule_day_key_fkey FOREIGN KEY (day_key) REFERENCES schedule.days(day_key) NOT VALID;


--
-- Name: schedule schedule_group_number_fkey; Type: FK CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.schedule
    ADD CONSTRAINT schedule_group_number_fkey FOREIGN KEY (group_number) REFERENCES personal.groups(group_number) NOT VALID;


--
-- Name: schedule schedule_subject_key_fkey; Type: FK CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.schedule
    ADD CONSTRAINT schedule_subject_key_fkey FOREIGN KEY (subject_key) REFERENCES schedule.subjects(subject_key) NOT VALID;


--
-- Name: schedule schedule_teachers_key_fkey; Type: FK CONSTRAINT; Schema: schedule; Owner: postgres
--

ALTER TABLE ONLY schedule.schedule
    ADD CONSTRAINT schedule_teachers_key_fkey FOREIGN KEY (teachers_key) REFERENCES personal.teachers(teachers_key) NOT VALID;


--
-- Name: SCHEMA builds; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA builds TO admins;
GRANT ALL ON SCHEMA builds TO teachers;
GRANT ALL ON SCHEMA builds TO students;


--
-- Name: SCHEMA personal; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA personal TO admins;
GRANT ALL ON SCHEMA personal TO teachers;
GRANT ALL ON SCHEMA personal TO students;


--
-- Name: SCHEMA report; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA report TO admins;
GRANT ALL ON SCHEMA report TO teachers;
GRANT ALL ON SCHEMA report TO students;


--
-- Name: SCHEMA schedule; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA schedule TO admins;
GRANT ALL ON SCHEMA schedule TO teachers;
GRANT ALL ON SCHEMA schedule TO students;


--
-- Name: TABLE auditoriums; Type: ACL; Schema: builds; Owner: postgres
--

GRANT ALL ON TABLE builds.auditoriums TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE builds.auditoriums TO teachers;
GRANT SELECT ON TABLE builds.auditoriums TO students;


--
-- Name: TABLE groups; Type: ACL; Schema: personal; Owner: postgres
--

GRANT ALL ON TABLE personal.groups TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE personal.groups TO teachers;
GRANT SELECT ON TABLE personal.groups TO students;


--
-- Name: TABLE students; Type: ACL; Schema: personal; Owner: postgres
--

GRANT ALL ON TABLE personal.students TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE personal.students TO teachers;
GRANT SELECT ON TABLE personal.students TO students;


--
-- Name: TABLE teachers; Type: ACL; Schema: personal; Owner: postgres
--

GRANT ALL ON TABLE personal.teachers TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE personal.teachers TO teachers;
GRANT SELECT ON TABLE personal.teachers TO students;


--
-- Name: TABLE visiting; Type: ACL; Schema: report; Owner: postgres
--

GRANT ALL ON TABLE report.visiting TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE report.visiting TO teachers;
GRANT SELECT,INSERT,UPDATE ON TABLE report.visiting TO students;


--
-- Name: TABLE schedule; Type: ACL; Schema: schedule; Owner: postgres
--

GRANT ALL ON TABLE schedule.schedule TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE schedule.schedule TO teachers;
GRANT SELECT ON TABLE schedule.schedule TO students;


--
-- Name: TABLE subjects; Type: ACL; Schema: schedule; Owner: postgres
--

GRANT ALL ON TABLE schedule.subjects TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE schedule.subjects TO teachers;
GRANT SELECT ON TABLE schedule.subjects TO students;


--
-- Name: TABLE current_visiting; Type: ACL; Schema: report; Owner: postgres
--

GRANT ALL ON TABLE report.current_visiting TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE report.current_visiting TO teachers;
GRANT SELECT ON TABLE report.current_visiting TO students;


--
-- Name: TABLE work_tabel; Type: ACL; Schema: report; Owner: postgres
--

GRANT SELECT,INSERT,UPDATE ON TABLE report.work_tabel TO students;
GRANT SELECT,INSERT,UPDATE ON TABLE report.work_tabel TO teachers;


--
-- Name: TABLE days; Type: ACL; Schema: schedule; Owner: postgres
--

GRANT ALL ON TABLE schedule.days TO admins;
GRANT SELECT,INSERT,UPDATE ON TABLE schedule.days TO teachers;
GRANT SELECT ON TABLE schedule.days TO students;


--
-- PostgreSQL database dump complete
--

