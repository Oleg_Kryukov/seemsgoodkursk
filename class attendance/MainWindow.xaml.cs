﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
   using System.Device.Location; 


namespace class_attendance
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _connectString;
        public string role="";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonEnter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string connectionstring = new ConnectionString().Connect(loginTB.Text, passwordTB.Text);
                _connectString = connectionstring;
                NpgsqlConnection connection = new NpgsqlConnection(connectionstring);
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand("select check_role ()", connection);
                NpgsqlDataReader npgsqlDataReader = command.ExecuteReader();
                while (npgsqlDataReader.Read())
                {
                    role = npgsqlDataReader.GetValue(0).ToString();
                }
                connection.Close();
                if (role != "")
                {
                    if (role == "students")
                    {
                        st_en form1 = new st_en(ref connection);
                        Hide();
                        form1.Show();
                    }
                    if (role == "teachers")
                    {
                        pr_en form1 = new pr_en(ref connection);
                        Hide();
                        form1.Show();
                    }
                }
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.ToString());
                throw;
            }
        }
    }
}
