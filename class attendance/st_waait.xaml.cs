﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace class_attendance
{
    /// <summary>
    /// Interaction logic for st_waait.xaml
    /// </summary>
    public partial class st_waait : Window
    {
        NpgsqlConnection connection;
        public st_waait(ref NpgsqlConnection con)
        {
            connection = con;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string teacher = string.Empty, subject_name = string.Empty, corpus = string.Empty, auditorium = string.Empty;

            connection.Open();

            NpgsqlCommand command = new NpgsqlCommand("select * FROM next_lesson_student()", connection);
            NpgsqlDataReader npgsqlDataReader = command.ExecuteReader();
            while (npgsqlDataReader.Read())
            {
                teacher = npgsqlDataReader.GetValue(0).ToString();
                subject_name = npgsqlDataReader.GetValue(1).ToString();
                corpus = npgsqlDataReader.GetValue(2).ToString();
                auditorium = npgsqlDataReader.GetValue(3).ToString();
            }

            connection.Close();
            connection.Open();

            lessonTB.Text = subject_name;
            audTB.Text = auditorium;
            CorpTB.Text = corpus;
            teachTB.Text = teacher;

            DataGridTextColumn textColumn = new DataGridTextColumn();
            textColumn.Header = "Time";
            dataGrid.Columns.Add(textColumn);
            DataGridTextColumn textColumn1 = new DataGridTextColumn();
            textColumn1.Header = "Subject";
            dataGrid.Columns.Add(textColumn1);

            command = new NpgsqlCommand("select * FROM current_schedule_student()", connection);
            npgsqlDataReader = command.ExecuteReader();
            while (npgsqlDataReader.Read())
            {
                string[] arr = new string[npgsqlDataReader.FieldCount];
                arr[0] = npgsqlDataReader.GetValue(0).ToString();
                arr[1] = npgsqlDataReader.GetValue(1).ToString();
                dataGrid.Items.Add(arr);
            }

            connection.Close();
        }
    }
}
