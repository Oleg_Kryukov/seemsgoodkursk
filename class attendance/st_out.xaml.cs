﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace class_attendance
{
    /// <summary>
    /// Interaction logic for st_out.xaml
    /// </summary>
    public partial class st_out : Window
    {
        NpgsqlConnection connection;
        public st_out(ref NpgsqlConnection con)
        {
            connection = con;
            InitializeComponent();
        }

        private void buttonCheckO_Click(object sender, RoutedEventArgs e)
        {
            string geo = "'(510,510,510)'::\"position\"";

            connection.Open();

            NpgsqlCommand command = new NpgsqlCommand("select tap_end (" + geo + ")", connection);
            NpgsqlDataReader npgsqlDataReader = command.ExecuteReader();

            connection.Close();

            st_waait form = new st_waait(ref connection);
            form.Show();
            Hide();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string teacher = string.Empty, subject_name = string.Empty, corpus = string.Empty, auditorium = string.Empty;

            connection.Open();

            NpgsqlCommand command = new NpgsqlCommand("select * FROM current_lesson_student()", connection);
            NpgsqlDataReader npgsqlDataReader = command.ExecuteReader();
            while (npgsqlDataReader.Read())
            {
                teacher = npgsqlDataReader.GetValue(0).ToString();
                subject_name = npgsqlDataReader.GetValue(1).ToString();
                corpus = npgsqlDataReader.GetValue(2).ToString();
                auditorium = npgsqlDataReader.GetValue(3).ToString();
            }

            connection.Close();

            lessonTB.Text = subject_name;
            audTB.Text = auditorium;
            CorpTB.Text = corpus;
            teachTB.Text = teacher;
        }
    }
}
